package com.portfoliomgmt.marketdatasimulator.config;

import com.portfoliomgmt.marketdatasimulator.model.StockQuote;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
public class KafkaConfiguration {

    Logger logger = LoggerFactory.getLogger(KafkaConfiguration.class);

    @Bean
    public ProducerFactory<String, StockQuote> producerFactory() {
        logger.info("Will attempt to connect: "+  bootstrapServers);
        Map<String, Object> factoryConfig = new HashMap<>();
        factoryConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        factoryConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        factoryConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        return new DefaultKafkaProducerFactory<>(factoryConfig);
    }

    @Bean
    public KafkaTemplate<String, StockQuote> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }


    @Value("${bootstrap.servers}")
    private String bootstrapServers;
}
