package com.portfoliomgmt.marketdatasimulator.batch.processor;

import com.portfoliomgmt.marketdatasimulator.domain.MarketData;
import com.portfoliomgmt.marketdatasimulator.model.MarketDataDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class MarketDataItemProcessor implements ItemProcessor<MarketDataDetail, MarketData> {
    private static final Logger log = LoggerFactory.getLogger(MarketDataItemProcessor.class);

    @Autowired
    private DateTimeFormatter dateTimeFormatter;

    @Override
    public MarketData process(MarketDataDetail marketDataDetail) throws Exception {


        MarketData transformed = new MarketData();
        transformed.setSymbol(marketDataDetail.getSymbol());
        transformed.setSeries(marketDataDetail.getSeries());
        transformed.setOpen(marketDataDetail.getOpen());
        transformed.setHigh(marketDataDetail.getHigh());
        transformed.setLow(marketDataDetail.getLow());
        transformed.setClose(marketDataDetail.getClose());
        transformed.setLast(marketDataDetail.getLast());
        transformed.setPrevClose(marketDataDetail.getPrevClose());
        transformed.setTotTrdQty(marketDataDetail.getTotTrdQty());
        transformed.setTotTrdVal(marketDataDetail.getTotTrdVal());
        transformed.setTimeStamp(LocalDate.parse(marketDataDetail.getTimestamp(),dateTimeFormatter));
        transformed.setTotalTrades(marketDataDetail.getTotalTrades());
        transformed.setIsin(marketDataDetail.getIsin());


        return transformed;
    }
}
