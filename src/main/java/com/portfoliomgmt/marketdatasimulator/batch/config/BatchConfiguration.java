package com.portfoliomgmt.marketdatasimulator.batch.config;

import com.portfoliomgmt.marketdatasimulator.batch.processor.MarketDataItemProcessor;
import com.portfoliomgmt.marketdatasimulator.domain.MarketData;
import com.portfoliomgmt.marketdatasimulator.model.MarketDataDetail;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.batch.item.data.builder.MongoItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private JobExecutionListenerSupport jobCompletionNotificationListener;

    @Autowired
    private DelimitedLineAggregator<String> lineAggregator;

    @Value("${simulator.input.datafile}")
    private String dataFileLocation;

    @Value("${simulator.out.datafile}")
    private String dataFileOutLocation;


    @Bean
    @StepScope
    public FlatFileItemReader<MarketDataDetail> dataReader() {

        return new FlatFileItemReaderBuilder<MarketDataDetail>().name("marketDataReader")
                .resource(new FileSystemResource(dataFileOutLocation+"/data_out.csv")).delimited()
                .names("symbol", "series", "open", "high", "low", "close", "last",
                        "prevClose", "totTrdQty", "totTrdVal", "timeStamp", "totalTrades", "isin")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<>() {
                    {
                        setTargetType(MarketDataDetail.class);
                    }
                }).linesToSkip(1).build();
    }

    @Bean
    @StepScope
    public FlatFileItemReader<String> dataMassager() {

         return new FlatFileItemReaderBuilder<String>().name("marketDataReader")
                .resource(new FileSystemResource(dataFileLocation+"/data.csv"))
                .lineMapper((String line, int lineNumber) -> line.substring(0, line.length() - 1)).build();

    }

    @Bean
    @StepScope
    public FlatFileItemWriter<String> flatFileWriter() {
        return new FlatFileItemWriterBuilder<String>().name("massagedWriter")
                .shouldDeleteIfExists(true).resource(new FileSystemResource(dataFileOutLocation+"/data_out.csv"))
                .lineAggregator(lineAggregator)
                .build();
    }

    @Bean
    @StepScope
    public MongoItemWriter<MarketData> dataWriter() {
        mongoTemplate.dropCollection("market_data");
        return new MongoItemWriterBuilder<MarketData>().template(mongoTemplate).collection("market_data")
                .build();
    }

    @Bean
    @StepScope
    public MarketDataItemProcessor processor() {
        return new MarketDataItemProcessor();
    }

    @Bean
    public Step writeMarketData() throws Exception {

        return this.stepBuilderFactory.get("writeMarketData").<MarketDataDetail, MarketData>chunk(5)
                .reader(dataReader())
                .processor(processor()).writer(dataWriter()).build();
    }

    @Bean
    public Step massageDataFile() throws Exception {

        return this.stepBuilderFactory.get("massageDataFile").<String, String>chunk(5)
                .reader(dataMassager()).writer(flatFileWriter())
                .build();
    }

    @Bean
    public Job updateUserJob() throws Exception {

        return this.jobBuilderFactory.get("updateMarketDataJob").incrementer(new RunIdIncrementer())
                .listener(jobCompletionNotificationListener)
                .start(massageDataFile())
                .next(writeMarketData()).build();
    }
}
