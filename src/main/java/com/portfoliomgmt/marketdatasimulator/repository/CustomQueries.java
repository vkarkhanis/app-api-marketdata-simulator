package com.portfoliomgmt.marketdatasimulator.repository;

import com.portfoliomgmt.marketdatasimulator.domain.MarketData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomQueries {

    public List<String> findAllSymbol() {
        return mongoTemplate.query(MarketData.class).distinct("symbol").as(String.class).all();
    }

    @Autowired
    private MongoTemplate mongoTemplate;
}
