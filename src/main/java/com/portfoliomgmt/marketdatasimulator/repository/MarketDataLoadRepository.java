package com.portfoliomgmt.marketdatasimulator.repository;

import com.portfoliomgmt.marketdatasimulator.domain.MarketData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarketDataLoadRepository extends MongoRepository<MarketData, String> {

    MarketData findBySymbol(String symbol);


}
