package com.portfoliomgmt.marketdatasimulator.model;

public enum Exchange {
    NSE, BSE, NASDAQ, NYSE
}
