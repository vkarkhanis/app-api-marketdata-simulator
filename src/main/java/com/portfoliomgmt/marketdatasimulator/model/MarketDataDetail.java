package com.portfoliomgmt.marketdatasimulator.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MarketDataDetail {

    private String symbol;
    private String series;
    private BigDecimal open;
    private BigDecimal high;
    private BigDecimal low;
    private BigDecimal close;
    private BigDecimal last;
    private BigDecimal prevClose;
    private long totTrdQty;
    private BigDecimal totTrdVal;
    private String timestamp;
    private long totalTrades;
    private String isin;

}
