package com.portfoliomgmt.marketdatasimulator.model;

import lombok.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

@NoArgsConstructor @Getter @AllArgsConstructor @ToString
public class StockQuote {
    private String symbol;
    private BigDecimal high;
    private BigDecimal low;
    private BigDecimal open;
    private BigDecimal price;
    private BigInteger volume;
    private LocalDate lastTradingDay;
    private BigDecimal previousClose;
    private double change;
    private String changePercent;

}
