package com.portfoliomgmt.marketdatasimulator.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum InstrumentType {
    MF("Mutual Funds", "MF"), EQUITY("Equity", "EQ"),
    OPTIONS("Options", "OP"), FUTURES("Futures", "FUT");

    private String description;
    private String shortName;
}
