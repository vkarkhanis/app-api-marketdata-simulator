package com.portfoliomgmt.marketdatasimulator.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Document(collection = "market_data")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketData implements Serializable {

    private static final long serialVersionUID = -288060070504986217L;

    @Id
    private String symbol;
    private String series;
    private BigDecimal open;
    private BigDecimal high;
    private BigDecimal low;
    private BigDecimal close;
    private BigDecimal last;
    private BigDecimal prevClose;
    private long totTrdQty;
    private BigDecimal totTrdVal;
    @Field("eodDate")
    private LocalDate timeStamp;
    private long totalTrades;
    private String isin;
    @CreatedDate
    private LocalDateTime createdAt;

}
