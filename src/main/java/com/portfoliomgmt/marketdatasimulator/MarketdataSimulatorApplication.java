package com.portfoliomgmt.marketdatasimulator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableScheduling
@EnableCaching
public class MarketdataSimulatorApplication {

	private static final Logger logger = LoggerFactory.getLogger(MarketdataSimulatorApplication.class);

	public static void main(String[] args) {

		logger.info("Starting Market Data Producer");
		SpringApplication.run(MarketdataSimulatorApplication.class, args);
	}

}
