package com.portfoliomgmt.marketdatasimulator.services;

import com.portfoliomgmt.marketdatasimulator.components.MarketDataCache;
import com.portfoliomgmt.marketdatasimulator.components.MarketDataCacheImpl;
import com.portfoliomgmt.marketdatasimulator.domain.MarketData;
import com.portfoliomgmt.marketdatasimulator.model.StockQuote;
import com.portfoliomgmt.marketdatasimulator.repository.MarketDataLoadRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.List;

@Service
public class KafkaPublisherService {

    private static final Logger logger = LoggerFactory.getLogger(KafkaPublisherService.class);

    @Scheduled(cron="0/20 * 9-15 * * MON-FRI", zone="Asia/Kolkata")
    public void simulateStockPrices() {
        logger.info("Start simulation: ");

        List<String> allSymbols = marketDataCache.getAllSymbols();

        for (String eachSymbol : allSymbols) {
            MarketData data = marketDataCache.getDataBySymbol(eachSymbol);

            kafkaTemplate.send(
                topicName, data.getSymbol(),
                new StockQuote(data.getSymbol(), data.getHigh(), data.getLow(), data.getOpen(),
                        getRandom(BigDecimal.valueOf(1.15).multiply(data.getLast()),
                                BigDecimal.valueOf(0.85).multiply(data.getLast())).setScale(6, RoundingMode.HALF_EVEN),

                        BigInteger.valueOf(Math.round(getRandom(
                                BigDecimal.valueOf(1.15).multiply(new BigDecimal(data.getTotTrdQty())),
                                BigDecimal.valueOf(0.85).multiply(new BigDecimal(data.getTotTrdQty()))).doubleValue())),

                        data.getTimeStamp(),data.getPrevClose(), 1.0,"1%")
                );
        }
    }

    private BigDecimal getRandom(@Nullable BigDecimal max, @Nullable BigDecimal min) {
        if (max == null && min == null) return BigDecimal.valueOf(Math.random());
        if (max == null) return BigDecimal.valueOf(Math.random()).multiply(min).add(min);
        if (min == null) return BigDecimal.valueOf(Math.random()).multiply(max);

        return BigDecimal.valueOf(Math.random()).multiply(max.subtract(min)).add(min);
    }

    private BigDecimal getRandomRounded(@Nullable BigDecimal max, @Nullable BigDecimal min) {
        return getRandom(max, min).setScale(0, RoundingMode.FLOOR);
    }

    @Autowired
    private KafkaTemplate<String, StockQuote> kafkaTemplate;

    @Value("${ind.topic.name}")
    private String topicName;

    @Autowired
    private MarketDataLoadRepository marketDataLoadRepository;

    @Autowired
    private MarketDataCacheImpl marketDataCacheImpl;

    @Autowired
    @Qualifier("marketDataCacheImpl")
    private MarketDataCache marketDataCache;


}
