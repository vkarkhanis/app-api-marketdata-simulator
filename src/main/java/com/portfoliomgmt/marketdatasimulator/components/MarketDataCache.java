package com.portfoliomgmt.marketdatasimulator.components;

import com.portfoliomgmt.marketdatasimulator.domain.MarketData;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

public interface MarketDataCache {

    @Cacheable(value="symbols", key="'symbols'", unless="#result==null or #result.isEmpty()")
    List<String> getAllSymbols();

    @Cacheable(value="marketData", key="#symbol", unless="#result==null")
    MarketData getDataBySymbol(String symbol);

    @CachePut(value="marketData", key="#data.getSymbol()")
    default MarketData updateMarketData(MarketData data) {
        return data;
    }

    void refreshSymbols();
}
