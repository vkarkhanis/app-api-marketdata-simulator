package com.portfoliomgmt.marketdatasimulator.components;

import com.portfoliomgmt.marketdatasimulator.MarketdataSimulatorApplication;
import com.portfoliomgmt.marketdatasimulator.domain.MarketData;
import com.portfoliomgmt.marketdatasimulator.repository.CustomQueries;
import com.portfoliomgmt.marketdatasimulator.repository.MarketDataLoadRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("marketDataCacheImpl")
public class MarketDataCacheImpl implements MarketDataCache {

    private static final Logger logger = LoggerFactory.getLogger(MarketDataCacheImpl.class);

    @Override
    public List<String> getAllSymbols() {
        logger.info("Fetching symbols from DB");
        return customQueries.findAllSymbol();
    }

    @Override
    public MarketData getDataBySymbol(String symbol) {
        return marketDataLoadRepository.findBySymbol(symbol);
    }

    @Override
    public void refreshSymbols() {
        getAllSymbols();
    }

    @Autowired
    private MarketDataLoadRepository marketDataLoadRepository;

    @Autowired
    private CustomQueries customQueries;
}
