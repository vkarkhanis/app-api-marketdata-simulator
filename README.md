# README #
### What is this repository for? ###

* This is the main simulator service which reads the last best available market data(actual) for a day and simulates on top of that data set.
  The idea is to simulate the market data and make it available for development purposes.
  Any available commercial market data today charges considerable amount of fee to get real-time quotes for various securities trading on exchanges.
* It may be worth paying this fee in production environment, but when developing the cost can be avoided by simulating the data and giving it a real-time nature
* This code uses one time good set of data (like EOD prices for all stocks) and randomize the constantly changing data honoring the tick size. 
* Further it runs a scheduler to randomize the data at a high frequency and stream it over Apache Kafka topic
* ### v1.0 ###
* # PLEASE NOTE: # 
* The data is not REAL or ACTUAL. It is simulated to mimic actual data for development purposes only


### How do I get set up? ###

* The code reads actual data once a day (EOD of previous day), stores in Mongo DB and a cache, randomizes few fields and streams at a high frequency on a Apache Kafka topic, thus simulating the real-time constantly changing market-data
* The entire service can be run using docker using following script, once checked out from GIT:
    1. mvn clean install
    2. docker-compose --env-file env/.env.<dev | qa | prod> down && docker-compose --env-file env/.env.<dev | qa | prod> build --pull && docker-compose --env-file env/.env.<dev | qa | prod> up -d
* Docker Desktop installation is needed to run the service locally. For official documentation of Docker Desktop, please refer to [HERE] https://docs.docker.com/desktop/ 
  * In order to peek the messages on the Kafka queue, follow the steps below (CLI apache Kafka consumer):
      1. #### In Docker container:  #### 
      docker run -it --rm --network mdsimulator confluentinc/cp-kafka /bin/kafka-console-consumer --bootstrap-server kafka:9092 --topic marketdata-ind
  
      2. #### On local desktop (change directory to Kafka installation directory):  #### 
        .\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic marketdata-ind --from-beginning

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* vkarkhanis@gmail.com